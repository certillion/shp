Shell Provisioner
=================

Ferramenta auxiliar para provisionamento e implantação com shell script.

Instalando
----------

1. Edite as variáveis do arquivo `configure`.
1. Execute `bash configure`.
1. Execute `make` para compilar.
1. Execute `make install` para instalar. Dependendo do diretório escolhido para instalação, você precisará usar `sudo`.
1. Crie a variável de ambiente **SHP_HOME** apontando para o local de instalação.

Configurando
------------

1. Entre em `$SHP_HOME/nodes`.
1. Crie um arquivo chamado `localhost`.
1. Insira as seguintes variáveis nesse arquivo:
    - **NODE_SHP_HOME**: Mesmo valor de `$SHP_HOME`.
    - **NODE_PROJECT**: Projeto de configuração do node.
    - **NODE_PROFILE**: Ambiente de execução do node.
    - **NODE_CLASSES**: Classes as quais o node pertence.
    - **NODE_USER**: Usuário usado para aplicar as configurações.
    - **NODE_APPLY_DIR**: Diretório base de aplicação da configuração.

Executando
----------

1. Na pasta raiz do seu projeto, crie um arquivo chamado `Manifest`. Esse arquivo deve definir as seguintes variáveis:
    - **PROJECT_NAME**
    - **PROJECT_VERSION**
1. Execute `shp <command> <args>`, onde '<command>' pode ser:
    - **package**: Monta o pacote de implantação.
    - **deploy**: Transfere o pacote de implantação para um node.
    - **apply**: Aplica a configuração em um node.
    - **update-node**: Atualiza as informações de um node.
1. As seguintes 'extensions' vêm instaladas por padrão:
    - **shp-version**: Durante o package, a pasta será removida se o seu nome for diferente da versão.
    - **shp-get**: Durante o apply, o arquivo será lido e usado como URL para download.
    - **shp-template**: Durante o apply, o conteúdo do arquivo será interpolado com as variáveis de ambiente.
    - **shp-script**: Durante o apply, o arquivo será executado.

Estrutura do projeto
--------------------

Um projeto SHP serve para criar um pacote de implantação de um sistema, bem como facilitar sua aplicação e o gerenciamento da configuração. Em um projeto SHP, o código deve estar na pasta `src` e o pacote de implantação é criado na pasta `target`.

Na pasta `src`, o código deve ter a seguinte estrutura: `<profile>/<class>`. Onde:

- **profile**: Nome que representa o ambiente em que o código será executado.
- **class**: Classes de nodes. Cada node pode pertencer a uma ou mais classes.

Na pasta `target`, o pacote terá o nome `<project>-<version>-<profile>`.

A keyword **common** indica algo que é comum a todos os profiles ou a todas as classes. Durante o build é feito merge do profile sendo construído com o profile 'common' e de cada classe com a classe 'common'.